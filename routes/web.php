<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
//    return redirect()->route('login');
});

Route::middleware(['auth'])->group(function () {

    Route::get('index', function (){
        return view('index');
    })->name('home.index');

    ///// Super admin routes /////
    Route::group(['prefix'=>'admin','middleware'=>'super_admin'], function (){

        Route::resources([
            'admin'=>\App\Http\Controllers\AdminController::class]);

    });

    ///// Admin routes /////
    Route::group(['prefix'=>'admin','middleware'=>'admin'], function (){

        Route::resource('admin', \App\Http\Controllers\AdminController::class)->only([
            'index', 'show','edit','update'
        ]);

    });

    ///// Manager routes /////
    Route::group(['prefix'=>'admin','middleware'=>'manager'], function (){

        Route::resource('admin', \App\Http\Controllers\AdminController::class)->only([
            'show','edit','index'
        ]);

        Route::resource('buyer', \App\Http\Controllers\BuyerController::class)->only([
            'index'
        ]);
    });


    Route::resource('dashboard',\App\Http\Controllers\DashboardController::class);
    Route::resource('phone',\App\Http\Controllers\PhoneNumberController::class);
    Route::resource('sms',\App\Http\Controllers\SmsController::class);
    Route::resource('sms_service',\App\Http\Controllers\SmsServiceController::class);
    Route::resource('sms_text',\App\Http\Controllers\SmsTextController::class);
    Route::resource('region',\App\Http\Controllers\RegionController::class);

    Route::get('excel/export',[\App\Http\Controllers\PhoneNumberController::class,'export'])->name('phone.export');

});


//Auth::routes([
//    'register' => false, // Registration Routes...
//    'reset' => false, // Password Reset Routes...
//    'verify' => false, // Email Verification Routes...
//]);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
