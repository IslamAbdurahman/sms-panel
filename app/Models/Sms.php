<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class Sms extends Model
{
    use HasFactory;
    public $timestamps=false;

    protected $fillable = [
        'number_id',
        'text_id',
        'date',
        'service_id',
        'status',
    ];

    public static function sysdc($num, $message){
        $url = 'https://sms.sysdc.ru/index.php';

        $token = DB::select("select * from sms_services where name = 'sysdc'");

        $res = Http::withToken($token[0]->token)
            ->attach('mobile_phone', $num)
            ->attach('message', $message)
            ->post($url);

        return $res->body();
    }

    public static function play_mobile($num, $message){

        $service = DB::select("select * from sms_services where name = 'play_mobile'");

         $data = [
             'recipient'=>$num,
             'message-id'=>'abc000000001',
             'sms'=> [
                 'originator'=>'3700',
                 'content'=>[
                     'text'=> $message
                 ]
             ],
         ];

     $key = $service[0]->key;
     $secret = $service[0]->secret;
     $url = 'http://91.204.239.44/broker-api/send';

         $res = \Illuminate\Support\Facades\Http::withBasicAuth($key, $secret)
             ->post($url, [
                 'messages' => $data
             ]);

        return $res->body();
    }


}
