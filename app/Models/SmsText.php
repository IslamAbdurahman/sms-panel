<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsText extends Model
{
    use HasFactory;
    public $timestamps=false;

    protected $table = 'sms_text';
    protected $fillable = [
        'text',
        'date',
    ];
}
