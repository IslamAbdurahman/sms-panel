<?php

namespace App\Exports;

use App\Models\PhoneNumber;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class PhoneNumberExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
//        $phones = DB::table('phone_numbers as pn')
//            ->join('regions as r','r.id','=','pn.region_id')
//            ->leftJoin('sms as s','s.number_id','=','pn.id')
//            ->select('pn.*','r.name as region',
//                DB::raw('COUNT(s.id) as sms'))
//            ->groupBy('pn.id')
//            ->get();
        $phones = PhoneNumber::all();
        return $phones;
    }
}
