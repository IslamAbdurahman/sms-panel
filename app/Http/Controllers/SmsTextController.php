<?php

namespace App\Http\Controllers;

use App\Models\Region;
use App\Models\SmsService;
use App\Models\SmsText;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SmsTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->from && $request->to){
            $sms_text = DB::table('sms_text as st')
                ->leftJoin('sms as s','s.text_id','=','st.id')
                ->whereBetween('s.date',[$request->from, $request->to.' 23:59:59'])
                ->select('st.*',DB::raw('COUNT(s.id) as sms'))
                ->groupBy('st.id')
                ->orderBy('st.id','desc')
                ->paginate(10);
            $from = $request->from;
            $to = $request->to;
        }else{
            $sms_text = DB::table('sms_text as st')
                ->leftJoin('sms as s','s.text_id','=','st.id')
                ->select('st.*',DB::raw('COUNT(s.id) as sms'))
                ->groupBy('st.id')
                ->orderBy('st.id','desc')
                ->paginate(10);
            $from = date('Y-m-d');
            $to = date('Y-m-d');
        }


        return view('admin.sms_text.index', compact('sms_text','from','to'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sms_text = new SmsText;
        $sms_text->text = $request->text;
        $sms_text->save();

        return redirect()->route('sms_text.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->search && $request->from && $request->to){
            $messages = DB::table('sms as s')
                ->join('phone_numbers as pn','pn.id','=','s.number_id')
                ->join('sms_services as ss','ss.id','=','s.service_id')
                ->join('sms_text as st','st.id','=','s.text_id')
                ->where('s.text_id','=',$id)
                ->where('s.text','like','%'.$request->search.'%')
                ->whereBetween('s.date',[$request->from, $request->to.' 23:59:59'])
                ->select('pn.number','st.text','s.date','s.status','ss.name')
                ->orderBy('s.date','desc')
                ->paginate(10);
            $search = $request->search;
            $from = $request->from;
            $to = $request->to;
        }else if ($request->from && $request->to){
            $messages = DB::table('sms as s')
                ->join('phone_numbers as pn','pn.id','=','s.number_id')
                ->join('sms_services as ss','ss.id','=','s.service_id')
                ->join('sms_text as st','st.id','=','s.text_id')
                ->where('s.text_id','=',$id)
                ->whereBetween('s.date',[$request->from, $request->to.' 23:59:59'])
                ->select('pn.number','st.text','s.date','s.status','ss.name')
                ->orderBy('s.date','desc')
                ->paginate(10);
            $search = $request->search;
            $from = $request->from;
            $to = $request->to;
        }else{
            $messages = DB::table('sms as s')
                ->join('phone_numbers as pn','pn.id','=','s.number_id')
                ->join('sms_services as ss','ss.id','=','s.service_id')
                ->join('sms_text as st','st.id','=','s.text_id')
                ->where('s.text_id','=',$id)
                ->select('pn.number','st.text','s.date','s.status','ss.name')
                ->orderBy('s.date','desc')
                ->paginate(10);
            $search = '';
            $from = date('Y-m-d');
            $to = date('Y-m-d');
        }

        ///// sms controller index  ////

        if ($request->per_page){
            $per_page = $request->per_page;
        }else{
            $per_page = 10;
        }

        if ($request->search && $request->region != 0){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->where('pn.number','like','%'.$request->search.'%')
                ->where('pn.region_id','=',$request->region)
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = $request->search;
        }elseif ($request->search){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->where('pn.number','like','%'.$request->search.'%')
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = $request->search;
        }elseif ($request->region != 0){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->where('pn.region_id','=',$request->region)
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = '';
        }else{
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = '';

        }

        $region_id = $request->region;
        $regions = Region::all();
        $sms_services = SmsService::all();
        $sms_text = SmsText::all();


        return view('admin.sms_text.show',compact('messages','id','search','from','to',
            'phones','regions','region_id','per_page','sms_services','sms_text','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'text'=>'required'
        ]);

        $sms_text = SmsText::find($id);
        $sms_text->text = $request->text;
        $sms_text->update();

        return redirect()->route('sms_text.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sms_text_ = SmsText::find($id);
        $sms_text = DB::table('sms_text as st')
            ->leftJoin('sms as s','s.text_id','=','st.id')
            ->select('st.*',DB::raw('COUNT(s.id) as sms'))
            ->where('st.id','=',$id)
            ->groupBy('st.id')
            ->first();
        if ($sms_text->sms == 0){
            $sms_text_->delete();
        }

        return redirect()->route('sms_text.index');
    }
}
