<?php

namespace App\Http\Controllers;

use App\Models\SmsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SmsServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->from && $request->to){
            $sms_services = DB::table('sms_services as ss')
                ->leftJoin('sms as s','s.service_id','=','ss.id')
                ->whereBetween('s.date',[$request->from, $request->to.' 23:59:59'])
                ->select('ss.*',DB::raw('COUNT(s.id) as sms'))
                ->groupBy('ss.id')
                ->paginate(10);
            $from = $request->from;
            $to = $request->to;
        }else{
            $sms_services = DB::table('sms_services as ss')
                ->leftJoin('sms as s','s.service_id','=','ss.id')
                ->select('ss.*',DB::raw('COUNT(s.id) as sms'))
                ->groupBy('ss.id')
                ->paginate(10);
            $from = date('Y-m-d');
            $to = date('Y-m-d');
        }


        return view('admin.sms_service.index', compact('sms_services','from','to'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $sms_service = SmsService::find($id);
        $sms_service->token = $request->token;
        $sms_service->key = $request->key;
        $sms_service->secret = $request->secret;
        $sms_service->update();

        return redirect()->route('sms_service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
