<?php

namespace App\Http\Controllers;

use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->search){
            $regions = DB::table('count_number as cn')
                ->leftJoin('count_sms as cs','cs.region_id','=','cn.id')
                ->select('cn.*', 'cs.sms' )
                ->where('cn.name','like','%'.$request->search.'%')
                ->paginate(10);
            $search = $request->search;
        }else{
            $regions = DB::table('count_number as cn')
                ->leftJoin('count_sms as cs','cs.region_id','=','cn.id')
                ->select('cn.*', 'cs.sms' )
                ->paginate(10);
            $search = '';
        }


        return view('admin.region.index',compact('regions','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required'
        ]);
        $region = Region::create([
            'name'=>$request->name
        ]);

        return redirect()->route('region.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required'
        ]);
        $region = Region::find($id);
        $region->name = $request->name;
        $region->update();

        return redirect()->route('region.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $region = Region::find($id);
        $pn = DB::select('select * from phone_numbers where region_id = '.$id);
        if (count($pn)==0){
            $region->delete();
        }

        return redirect()->route('region.index');
    }
}
