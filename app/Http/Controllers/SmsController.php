<?php

namespace App\Http\Controllers;

use App\Imports\PhoneNumberImport;
use App\Models\PhoneNumber;
use App\Models\Region;
use App\Models\Sms;
use App\Models\SmsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\SmsText;
class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->per_page){
            $per_page = $request->per_page;
        }else{
            $per_page = 10;
        }

        if ($request->search && $request->region != 0){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->where('pn.number','like','%'.$request->search.'%')
                ->where('pn.region_id','=',$request->region)
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = $request->search;
        }elseif ($request->search){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->where('pn.number','like','%'.$request->search.'%')
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = $request->search;
        }elseif ($request->region != 0){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->where('pn.region_id','=',$request->region)
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = '';
        }else{
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->leftJoin('sms as s','s.number_id','=','pn.id')
                ->select('pn.*','r.name as region',
                    DB::raw('COUNT(s.id) as count'))
                ->groupBy('pn.id')
                ->paginate($per_page);
            $search = '';

        }

        $region_id = $request->region;
        $regions = Region::all();
        $sms_services = SmsService::all();
        $sms_text = SmsText::orderBy('id','desc')->get();


        return view('admin.sms.index', compact('phones','regions',
                'search','region_id','per_page','sms_services','sms_text'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $sms_text = SmsText::find($request->text_id);
        date_default_timezone_set("Asia/Tashkent");


        if ($request->sms_service && $request->sms_list){
            $numbers = DB::table('phone_numbers as pn')
                ->leftJoin('sms as s', function($q) use ($sms_text)
                {
                    $q->on('s.number_id', '=', 'pn.id')
                        ->where('s.text_id', '=', $sms_text->id);
                })
                ->where('s.id','=',null)
                ->whereIn('pn.id',$request->sms_list)
                ->select('pn.*')
                ->groupBy('pn.id')
                ->get();

//            dd($numbers);

            set_time_limit(30000000);
            if ($request->sms_service == 'sysdc'){
                foreach ($numbers as $number){
                    $sms = Sms::sysdc($number->number,$sms_text->text);
                    $json = json_decode($sms);
                    if ($json->status == 'waiting'){
                        Sms::create([
                            'number_id'=>$number->id,
                            'text_id'=>$request->text_id,
                            'date'=>date('Y-m-d H:i:s'),
                            'service_id'=>1,
                            'status'=>1
                        ]);
                    }else{
                        Sms::create([
                            'number_id'=>$number->id,
                            'text_id'=>$request->text_id,
                            'date'=>date('Y-m-d H:i:s'),
                            'service_id'=>1,
                            'status'=>0
                        ]);
                    }
                }
            }elseif ($request->sms_service == 'play_mobile'){
                foreach ($numbers as $number){
                    $sms = Sms::play_mobile($number->number,$sms_text->text);
                    if ($sms == 'Request is received'){
                        Sms::create([
                            'number_id'=>$number->id,
                            'text_id'=>$request->text_id,
                            'date'=>date('Y-m-d H:i:s'),
                            'service_id'=>2,
                            'status'=>1
                        ]);
                    }else{
                        Sms::create([
                            'number_id'=>$number->id,
                            'text_id'=>$request->text_id,
                            'date'=>date('Y-m-d H:i:s'),
                            'service_id'=>2,
                            'status'=>0
                        ]);
                    }
                }
            }

        }elseif ($request->sms_service && $request->region){

            $numbers = DB::table('phone_numbers as pn')
                ->leftJoin('sms as s', function($q) use ($sms_text)
                {
                    $q->on('s.number_id', '=', 'pn.id')
                        ->where('s.text_id', '=', $sms_text->id);
                })
                ->where('s.id','=',null)
                ->where('pn.region_id','=',$request->region)
                ->select('pn.*')
                ->paginate(10);

            set_time_limit(30000000);
            if ($request->sms_service == 'sysdc'){

                $n = $numbers->lastPage();
                for ($i = 1; $i <= $n; $i++) {
                    $phones = DB::table('phone_numbers as pn')
                        ->join('regions as r','r.id','=','pn.region_id')
                        ->select('pn.*','r.name as region')
                        ->paginate(10,'','',$i)->items();
                    foreach ($phones as $number){
                        $sms = Sms::sysdc($number->number,$sms_text->text);
                        $json = json_decode($sms);
                        if ($json->status)
                        if ($json->status == 'waiting'){
                            Sms::create([
                                'number_id'=>$number->id,
                                'text_id'=>$request->text_id,
                                'date'=>date('Y-m-d H:i:s'),
                                'service_id'=>1,
                                'status'=>1
                            ]);
                        }else{
                            Sms::create([
                                'number_id'=>$number->id,
                                'text_id'=>$request->text_id,
                                'date'=>date('Y-m-d H:i:s'),
                                'service_id'=>1,
                                'status'=>0
                            ]);
                        }
                    }
                }





            }elseif ($request->sms_service == 'play_mobile'){
                $n = $numbers->lastPage();
                for ($i = 1; $i <= $n; $i++) {
                    $phones = DB::table('phone_numbers as pn')
                        ->join('regions as r', 'r.id', '=', 'pn.region_id')
                        ->select('pn.*', 'r.name as region')
                        ->paginate(10, '', '', $i)->items();

                    foreach ($phones as $number) {
                        $sms = Sms::play_mobile($number->number, $sms_text->text);
                        if ($sms == 'Request is received') {
                            Sms::create([
                                'number_id' => $number->id,
                                'text_id' => $request->text_id,
                                'date' => date('Y-m-d H:i:s'),
                                'service_id' => 2,
                                'status' => 1
                            ]);
                        } else {

                            Sms::create([
                                'number_id' => $number->id,
                                'text_id' => $request->text_id,
                                'date' => date('Y-m-d H:i:s'),
                                'service_id' => 2,
                                'status' => 0
                            ]);
                        }
                    }
                }
            }
        }



        if ($request->show){
            return redirect()->route('sms_text.show',$request->show);
        }

        return redirect()->route('sms.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
