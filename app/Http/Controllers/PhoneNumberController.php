<?php

namespace App\Http\Controllers;

use App\Exports\PhoneNumberExport;
use App\Imports\PhoneNumberImport;
use App\Models\PhoneNumber;
use App\Models\Region;
use App\Models\Sms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PhoneNumberController extends Controller
{
    public function export()
    {

//        return Excel::raw(new PhoneNumberExport(), Excel::XLSX);

        return Excel::download(new PhoneNumberExport,date('Y-m-d H:i:s').'.xls');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->per_page){
            $per_page = $request->per_page;
        }else{
            $per_page = 10;
        }

        if ($request->search && $request->region != 0){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->where('pn.number','like','%'.$request->search.'%')
                ->where('pn.region_id','=',$request->region)
                ->select('pn.*','r.name as region')->paginate($per_page);
            $search = $request->search;
        }elseif ($request->search){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->where('pn.number','like','%'.$request->search.'%')
                ->select('pn.*','r.name as region')->paginate($per_page);
            $search = $request->search;
        }elseif ($request->region != 0){
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->where('pn.region_id','=',$request->region)
                ->select('pn.*','r.name as region')->paginate($per_page);
            $search = '';
        }else{
            $phones = DB::table('phone_numbers as pn')
                ->join('regions as r','r.id','=','pn.region_id')
                ->select('pn.*','r.name as region')
                ->paginate($per_page,'','','1');

            $search = '';
        }

        $region_id = $request->region;
        $regions = Region::all();

        return view('admin.phone.index', compact('phones','regions','search','region_id','per_page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->number && $request->region){
            $phone = PhoneNumber::firstOrCreate(['number' => $request->number]);
            $phone->number = $request->number;
            $phone->region_id = $request->region;
            $phone->save();
        }elseif ($request->file('excel') && $request->region){
            $array = Excel::toArray(new PhoneNumberImport, $request->file('excel'));

            foreach ($array[0] as $number){
                if ($number[1] != ''){
//                    dd($number[1]);
                    $phone = PhoneNumber::firstOrCreate(['number' => $number[1]]);
                    $phone->number = $number[1];
                    $phone->region_id = $request->region;
                    $phone->save();
                }
            }
        }



        return redirect()->route('phone.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->search && $request->from && $request->to){
            $messages = DB::table('sms as s')
                ->join('phone_numbers as pn','pn.id','=','s.number_id')
                ->join('sms_services as ss','ss.id','=','s.service_id')
                ->join('sms_text as st','st.id','=','s.text_id')
                ->where('s.number_id','=',$id)
                ->where('s.text','like','%'.$request->search.'%')
                ->whereBetween('s.date',[$request->from, $request->to.' 23:59:59'])
                ->select('pn.number','st.text','s.date','ss.name')
                ->orderBy('s.date','desc')
                ->paginate(10);
            $search = $request->search;
            $from = $request->from;
            $to = $request->to;
        }else if ($request->from && $request->to){
            $messages = DB::table('sms as s')
                ->join('phone_numbers as pn','pn.id','=','s.number_id')
                ->join('sms_services as ss','ss.id','=','s.service_id')
                ->join('sms_text as st','st.id','=','s.text_id')
                ->where('s.number_id','=',$id)
                ->whereBetween('s.date',[$request->from, $request->to.' 23:59:59'])
                ->select('pn.number','st.text','s.date','ss.name')
                ->orderBy('s.date','desc')
                ->paginate(10);
            $search = $request->search;
            $from = $request->from;
            $to = $request->to;
        }else{
            $messages = DB::table('sms as s')
                ->join('phone_numbers as pn','pn.id','=','s.number_id')
                ->join('sms_services as ss','ss.id','=','s.service_id')
                ->join('sms_text as st','st.id','=','s.text_id')
                ->where('s.number_id','=',$id)
                ->select('pn.number','st.text','s.date','ss.name')
                ->orderBy('s.date','desc')
                ->paginate(10);
            $search = '';
            $from = date('Y-m-d');
            $to = date('Y-m-d');
        }


        return view('admin.phone.show',compact('messages','id','search','from','to'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'number'=>'required',
            'region'=>'required',
        ]);
        $phone = PhoneNumber::find($id);
        $phone->number = $request->number;
        $phone->region_id = $request->region;
        $phone->update();


        return redirect()->route('phone.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone = PhoneNumber::find($id);
        $phone->delete();

        return redirect()->route('phone.index');
    }
}
