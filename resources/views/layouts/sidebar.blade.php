<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('public/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">
            @if(Auth::user()->role == 1)
                Super Admin
            @elseif(Auth::user()->role == 2)
                Admin
            @elseif(Auth::user()->role == 3)
                Manager
            @endif
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('public/storage/images/'.Auth::user()->image) }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('admin.edit', Auth::user()->id ) }}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="{{ route('dashboard.index') }}" class="nav-link {{ Request::path() == 'dashboard' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Dashboard
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link {{ request()->is('admin/admin','admin/buyer','admin/admin/*/edit') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Profile
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview active">
                        <li class="nav-item active">
                            <a href="{{ route('admin.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Adminlar</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('buyer.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Haridorlar</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('admin.edit',Auth::user()->id) }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tahrirlash</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="far fa-circle nav-icon"></i>
                                {{ __('Chiqish') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>

                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('phone.index') }}" class="nav-link {{ request()->is('phone','phone/*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-mobile"></i>
                        <p>
                            Tel Raqamlar
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('sms.index') }}" class="nav-link {{ request()->is('sms') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-sms"></i>
                        <p>
                            SMS
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('sms_text.index') }}" class="nav-link {{ request()->is('sms_text/*','sms_text') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-sms"></i>
                        <p>
                            SMS Text
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('sms_service.index') }}" class="nav-link {{ request()->is('sms_service') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-square"></i>
                        <p>
                            SMS Xizmatlar
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{ route('region.index') }}" class="nav-link {{ request()->is('region') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-house-user"></i>
                        <p>
                            Region
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>

                <li class="nav-header">Developer</li>
{{--                <li class="nav-item">--}}
{{--                    <a href="http://abdurahman.uz/" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-ellipsis-h"></i>--}}
{{--                        <p>Tabbed IFrame Plugin</p>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="nav-item">
                    <a href="http://abdurahman.uz/" target="_blank" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Documentation</p>
                    </a>
                </li>


            </ul>

        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

