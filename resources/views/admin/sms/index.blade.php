@extends('layouts.main')
@section('main-content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script >
        $(document).ready(function(){
            $(':checkbox[name="all"]').click(function(){
                if($(this).is(':checked')){
                    $("input:checkbox").prop("checked", true);
                } else {
                    $("input:checkbox").prop("checked", false);
                }
            });
        });
    </script>
    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">Telefon raqamlar</h3>
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('sms.index') }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf
                        <div class="input-group input-group-sm">
                            <select name="per_page" id="" class="form-control form-control-navbar">
                                <option {{ $per_page == 10 ? 'selected':'' }} value="10">10</option>
                                <option {{ $per_page == 20 ? 'selected':'' }}  value="20">20</option>
                                <option {{ $per_page == 50 ? 'selected':'' }}  value="50">50</option>
                                <option {{ $per_page == 100 ? 'selected':'' }}  value="100">100</option>
                            </select>
                            <select name="region" id="" class="form-control form-control-navbar">
                                <option value="0">Regionlar</option>
                                @foreach($regions as $region)
                                        <option {{ $region->id == $region_id ? 'selected':'' }} value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                            <input name="search" value="{{ $search }}" class="form-control form-control-navbar" type="search" placeholder="Qidirish" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <form action="{{ route('sms.store') }}" method="post">
                    @csrf

                <thead>
                <tr>
                    <th class="py-1">N
                        <input type="checkbox" name="all"  />
                    </th>
                    <th class="py-1">Tel nomer</th>
                    <th class="py-1">Region</th>
                    <th class="py-1">Action
                        <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#excel">
                            <i class="fas fa-sms"></i>
                        </button>
                        <div class="modal fade" id="excel">
                            <div class="modal-dialog">
                                <div class="modal-content bg-primary">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Excel raqamlar</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="exampleSelectBorder">SMS text</label>
                                                    <select name="text_id" id="" class="custom-select form-control-border" required>
                                                        <option selected disabled value="">SMS tanlang</option>
                                                        @foreach($sms_text as $item)
                                                            <option value="{{$item->id}}">{{$item->text}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleSelectBorder">SMS Srvice</label>
                                                    <select name="sms_service" id="" class="custom-select form-control-border" required>
                                                        <option selected disabled value="">Sms xizmat tanlang</option>
                                                        @foreach($sms_services as $sms)
                                                            <option value="{{ $sms->name }}">{{ $sms->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleSelectBorder">Region</label>
                                                    <select name="region" id="" class="custom-select form-control-border">
                                                        <option selected disabled value="">Region tanlang</option>
                                                        @foreach($regions as $region)
                                                            <option value="{{ $region->id }}">{{ $region->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                    <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Yopish</button>
                                            <button type="submit" class="btn btn-outline-light">Jo'natish</button>
                                        </div>

                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </th>
                </tr>
            </thead>


                    <tbody>
                        @foreach($phones as $phone)
                            <tr>
                                <td class="py-1">
                                    {{($phones->currentpage()-1)*$phones->perpage()+($loop->index+1)}}
                                    <input name="sms_list[]" value="{{ $phone->id }}" id="{ $phone->id }}" type="checkbox">

                                </td>
                                <td class="py-1">{{ $phone->number }}</td>
                                <td class="py-1">{{ $phone->region }}</td>
                                <td class="py-1">
                                    {{ $phone->count }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </form>


        </table>

        <div>{{ $phones->appends($_GET)->links() }}</div>
    </div>
@endsection
