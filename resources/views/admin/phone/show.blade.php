@extends('layouts.main')
@section('main-content')

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">Telefon raqam</h3>
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('phone.show', $id) }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf

                        <div class="input-group input-group-sm">
                            <input name="from" value="{{$from}}" class="form-control form-control-navbar" type="date" >
                            <input name="to" value="{{$to}}" class="form-control form-control-navbar" type="date" >
                        </div>

                        <div class="input-group input-group-sm">
                            <input name="search" value="{{$search}}" class="form-control form-control-navbar" type="search" placeholder="Qidirish" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th class="py-1">N</th>
                    <th class="py-1">Tel nomer</th>
                    <th class="py-1">Text</th>
                    <th class="py-1">Vaqt</th>
                    <th class="py-1">Sms xizmat</th>
                </tr>
            </thead>


                    <tbody>
                @foreach($messages as $message)
                    <tr>
                        <td class="py-1">
                            {{($messages->currentpage()-1)*$messages->perpage()+($loop->index+1)}}
                        </td>
                        <td class="py-1">{{ $message->number }}</td>
                        <td class="py-1">{{ $message->text }}</td>
                        <td class="py-1">{{ $message->date }}</td>
                        <td class="py-1">{{ $message->name }}</td>
                    </tr>
                @endforeach
            </tbody>


        </table>

        <div>{{ $messages->appends($_GET)->links() }}</div>
    </div>
@endsection
