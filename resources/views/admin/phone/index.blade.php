@extends('layouts.main')
@section('main-content')

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">Telefon raqamlar</h3>
                </li>
                <li>
                    <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#excel">
                        <i class="fas fa-upload "></i>
                    </button>
                    <div class="modal fade" id="excel">
                        <div class="modal-dialog">
                            <div class="modal-content bg-primary">
                                <form action="{{ route('phone.store') }}" method="post" enctype="multipart/form-data">

                                    <div class="modal-header">
                                        <h4 class="modal-title">Excel raqamlar</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Excel raqamlar</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" name="excel" class="custom-file-input" id="exampleInputFile" required>
                                                        <label class="custom-file-label" for="exampleInputFile">Excel yuklang</label>
                                                    </div>
                                                </div>
                                                <label for="exampleSelectBorder">Region</label>
                                                <select name="region" class="custom-select form-control-border" id="exampleSelectBorder" required>
                                                    <option  selected disabled value="">Viloyatlar</option>
                                                    @foreach($regions as $region)
                                                        <option value="{{ $region->id }}" >{{ $region->name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>

                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                    </div>

                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                    <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#create">
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="modal fade" id="create">
                        <div class="modal-dialog">
                            <div class="modal-content bg-primary">
                                <form action="{{ route('phone.store') }}" method="post" enctype="multipart/form-data">

                                    <div class="modal-header">
                                        <h4 class="modal-title">Excel raqamlar</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Telefon raqam</label>
                                                <div class="input-group">
                                                    <input type="number" name="number" class="custom-select form-control-border" required placeholder="Tel nomer">
                                                </div>
                                                <label for="exampleSelectBorder">Region</label>
                                                <select name="region" class="custom-select form-control-border" id="exampleSelectBorder" required>
                                                    <option  selected disabled value="">Viloyatlar</option>
                                                    @foreach($regions as $region)
                                                        <option value="{{ $region->id }}" >{{ $region->name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>

                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                    </div>

                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <a class="btn btn-outline-primary my-0 py-0 px-1 " href="{{ route('phone.export') }}">
                        <i class="fas fa-download"></i>
                    </a>
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('phone.index') }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf
                        <div class="input-group input-group-sm">
                            <select name="per_page" id="" class="form-control form-control-navbar">
                                <option {{ $per_page == 10 ? 'selected':'' }} value="10">10</option>
                                <option {{ $per_page == 20 ? 'selected':'' }}  value="20">20</option>
                                <option {{ $per_page == 50 ? 'selected':'' }}  value="50">50</option>
                                <option {{ $per_page == 100 ? 'selected':'' }}  value="100">100</option>
                            </select>
                            <select name="region" id="" class="form-control form-control-navbar">
                                <option value="0">Regionlar</option>
                                @foreach($regions as $region)
                                        <option {{ $region->id == $region_id ? 'selected':'' }} value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                            <input name="search" value="{{ $search }}" class="form-control form-control-navbar" type="search" placeholder="Qidirish" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th class="py-1">N</th>
                    <th class="py-1">Tel nomer</th>
                    <th class="py-1">Region</th>
                    <th class="py-1">Action

                    </th>
                </tr>
            </thead>


                    <tbody>
                @foreach($phones as $phone)
                    <tr>
                        <td class="py-1">
                            {{($phones->currentpage()-1)*$phones->perpage()+($loop->index+1)}}
                        </td>
                        <td class="py-1">
                            <a href="{{ route('phone.show', $phone->id) }}">{{ $phone->number }}</a>
                        </td>
                        <td class="py-1">{{ $phone->region }}</td>
                        <td class="py-1">
                            <button type="button" class="btn btn-danger my-0 py-0 px-1" data-toggle="modal" data-target="#modal-danger{{ $phone->id }}">
                                <i class="fas fa-trash m-0 p-0"></i>
                            </button>

                            <div class="modal fade" id="modal-danger{{ $phone->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-danger">
                                        <div class="modal-header">
                                            <h4 class="modal-title">O'chirish</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>O'chirishni xohlaysizmi?</p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <form class="d-inline" action="{{ route('phone.destroy',$phone->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-outline-light">
                                                    O'chirish
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <button type="button" class="btn btn-info my-0 py-0 px-1 " data-toggle="modal" data-target="#modal{{ $loop->index+1 }}">
                                <i class="fas fa-edit m-0 p-0"></i>
                            </button>
                            <div class="modal fade" id="modal{{ $loop->index+1 }}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-primary">
                                        <form action="{{ route('phone.update', $phone->id) }}" method="post" enctype="multipart/form-data">

                                            <div class="modal-header">
                                                <h4 class="modal-title">Admin Update</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                @csrf
                                                @method('PUT')
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Telefon raqam</label>
                                                        <input name="number" type="number" class="form-control" value="{{ $phone->number }}" required placeholder="Raqam kiriting">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleSelectBorder">Region</label>
                                                        <select name="region" class="custom-select form-control-border" id="exampleSelectBorder">
                                                            @foreach($regions as $region)
                                                                <option value="{{ $region->id }}" {{ $phone->region_id == $region->id ? 'selected':'' }}>{{ $region->name }}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                        <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                            </div>

                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </td>
                    </tr>
                @endforeach
            </tbody>


        </table>

        <div>{{ $phones->appends($_GET)->links() }}</div>
    </div>
@endsection
