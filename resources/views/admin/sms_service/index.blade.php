@extends('layouts.main')
@section('main-content')

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">SMS Xizmatlar</h3>
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('sms_service.index') }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf

                        <div class="input-group input-group-sm">
                            <input name="from" value="{{$from}}" class="form-control form-control-navbar" type="date" >
                            <input name="to" value="{{$to}}" class="form-control form-control-navbar" type="date" >
                        </div>

                        <div class="input-group input-group-sm">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th class="py-1">N</th>
                    <th class="py-1">Name</th>
                    <th class="py-1">Token</th>
                    <th class="py-1">Key</th>
                    <th class="py-1">Secret</th>
                    <th class="py-1">Count</th>
                    <th class="py-1">Action</th>
                </tr>
            </thead>


                    <tbody>
                @foreach($sms_services as $sms_service)
                    <tr>
                        <td class="py-1">
                            {{($sms_services->currentpage()-1)*$sms_services->perpage()+($loop->index+1)}}
                        </td>
                        <td class="py-1">{{ $sms_service->name }}</td>
                        <td class="py-1">{{ $sms_service->token }}</td>
                        <td class="py-1">{{ $sms_service->key }}</td>
                        <td class="py-1">{{ $sms_service->secret }}</td>
                        <td class="py-1">{{ $sms_service->sms }}</td>
                        <td class="py-1">

                            <button type="button" class="btn btn-info my-0 py-0 px-1 " data-toggle="modal" data-target="#modal{{ $loop->index+1 }}">
                                <i class="fas fa-edit m-0 p-0"></i>
                            </button>
                            <div class="modal fade" id="modal{{ $loop->index+1 }}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-primary">
                                        <form action="{{ route('sms_service.update', $sms_service->id) }}" method="post" enctype="multipart/form-data">

                                            <div class="modal-header">
                                                <h4 class="modal-title">{{ $sms_service->name }}</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                @csrf
                                                @method('PUT')
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Token</label>
                                                        <input name="token" type="text" class="form-control" value="{{ $sms_service->token }}" placeholder="Token kiriting">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Key</label>
                                                        <input name="key" type="text" class="form-control" value="{{ $sms_service->key }}" placeholder="Key kiriting">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Secret</label>
                                                        <input name="secret" type="text" class="form-control" value="{{ $sms_service->secret }}" placeholder="Secret kiriting">
                                                    </div>


                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                        <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                            </div>

                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </td>
                    </tr>
                @endforeach
            </tbody>


        </table>

        <div>{{ $sms_services->appends($_GET)->links() }}</div>
    </div>
@endsection
