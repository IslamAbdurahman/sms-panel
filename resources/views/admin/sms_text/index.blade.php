@extends('layouts.main')
@section('main-content')

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">SMS Xizmatlar</h3>
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('sms_text.index') }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf

                        <div class="input-group input-group-sm">
                            <input name="from" value="{{$from}}" class="form-control form-control-navbar" type="date" >
                            <input name="to" value="{{$to}}" class="form-control form-control-navbar" type="date" >
                        </div>

                        <div class="input-group input-group-sm">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th class="py-1">N</th>
                    <th class="py-1">Text</th>
                    <th class="py-1">Vaqt</th>
                    <th class="py-1">Sms</th>
                    <th class="py-1">
                        Action
                        <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#create-sms-text">
                            <i class="fas fa-plus m-0 p-0"></i>
                        </button>
                        <div class="modal fade" id="create-sms-text">
                            <div class="modal-dialog">
                                <div class="modal-content bg-primary">
                                    <form action="{{ route('sms_text.store') }}" method="post" enctype="multipart/form-data">

                                        <div class="modal-header">
                                            <h4 class="modal-title">Text yaratish</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            @csrf
                                            @method('post')
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Text</label>
                                                    <textarea name="text" id="" cols="30" class="form-control" placeholder="Text kiriting"></textarea>
                                                </div>


                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                    <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                        </div>

                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </th>
                </tr>
            </thead>


                    <tbody>
                @foreach($sms_text as $item)
                    <tr>
                        <td class="py-1">
                            {{($sms_text->currentpage()-1)*$sms_text->perpage()+($loop->index+1)}}
                        </td>
                        <td class="py-1">
                            <a href="{{ route('sms_text.show',$item->id) }}">{{ $item->text }}</a>
                        </td>
                        <td class="py-1">{{ $item->date }}</td>
                        <td class="py-1">{{ $item->sms }}</td>
                        <td class="py-1">

                            @if($item->sms == 0)

                            <button type="button" class="btn btn-danger my-0 py-0 px-1" data-toggle="modal" data-target="#modal-danger{{ $item->id }}">
                                <i class="fas fa-trash m-0 p-0"></i>
                            </button>

                            <div class="modal fade" id="modal-danger{{ $item->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-danger">
                                        <div class="modal-header">
                                            <h4 class="modal-title">O'chirish</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>O'chirishni xohlaysizmi?</p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <form class="d-inline" action="{{ route('sms_text.destroy',$item->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-outline-light">
                                                    O'chirish
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->


                            <button type="button" class="btn btn-info my-0 py-0 px-1 " data-toggle="modal" data-target="#modal{{ $loop->index+1 }}">
                                <i class="fas fa-edit m-0 p-0"></i>
                            </button>
                            <div class="modal fade" id="modal{{ $loop->index+1 }}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-primary">
                                        <form action="{{ route('sms_text.update', $item->id) }}" method="post" enctype="multipart/form-data">

                                            <div class="modal-header">
                                                <h4 class="modal-title">{{ $item->text }}</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                @csrf
                                                @method('PUT')
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Text</label>
                                                        <input name="text" type="text" class="form-control" value="{{ $item->text }}" placeholder="Text kiriting">
                                                    </div>


                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                        <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                            </div>

                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            @endif

                        </td>
                    </tr>
                @endforeach
            </tbody>


        </table>

        <div>{{ $sms_text->appends($_GET)->links() }}</div>
    </div>
@endsection
