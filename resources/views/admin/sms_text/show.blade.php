@extends('layouts.main')
@section('main-content')

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#send_sms" type="button" role="tab" aria-controls="home" aria-selected="true">Jo'natilgan Sms</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#others" type="button" role="tab" aria-controls="profile" aria-selected="false">Boshqa Raqamlar</button>
                            </li>
                        </ul>
                    </h3>
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('sms_text.show', $id) }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf

                        <div class="input-group input-group-sm">

                            <input name="from" value="{{$from}}" class="form-control form-control-navbar" type="date" >
                            <input name="to" value="{{$to}}" class="form-control form-control-navbar" type="date" >
                        </div>

                        <div class="input-group input-group-sm">
                            <input name="search" value="{{$search}}" class="form-control form-control-navbar" type="search" placeholder="Qidirish" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->



        <div class="card-body pt-0 table-responsive">



            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="send_sms" role="tabpanel" aria-labelledby="home-tab">
                    <table id="example1" class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th class="py-1">N</th>
                            <th class="py-1">Tel nomer</th>
                            <th class="py-1">Text</th>
                            <th class="py-1">Vaqt</th>
                            <th class="py-1">Sms xizmat</th>
                            <th class="py-1">Status</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($messages as $message)
                            <tr>
                                <td class="py-1">
                                    {{($messages->currentpage()-1)*$messages->perpage()+($loop->index+1)}}
                                </td>
                                <td class="py-1">{{ $message->number }}</td>
                                <td class="py-1">{{ $message->text }}</td>
                                <td class="py-1">{{ $message->date }}</td>
                                <td class="py-1">{{ $message->name }}</td>
                                <td class="py-1">{{ $message->status }}</td>
                            </tr>
                        @endforeach
                        </tbody>


                    </table>

                    <div>{{ $messages->appends($_GET)->links() }}</div>
                </div>
                <div class="tab-pane fade" id="others" role="tabpanel" aria-labelledby="profile-tab">

                    <table id="example1" class="table table-bordered table-striped ">
                        <form action="{{ route('sms.store') }}" method="post">
                            <input name="show" value="{{$id}}" type="hidden">
                            @csrf

                            <thead>
                            <tr>
                                <th class="py-1">N
                                    <input type="checkbox" name="all"  />
                                </th>
                                <th class="py-1">Tel nomer</th>
                                <th class="py-1">Region</th>
                                <th class="py-1">Action
                                    <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#excel">
                                        <i class="fas fa-sms"></i>
                                    </button>
                                    <div class="modal fade" id="excel">
                                        <div class="modal-dialog">
                                            <div class="modal-content bg-primary">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Excel raqamlar</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <label for="exampleSelectBorder">SMS text</label>
                                                            <select name="text_id" id="" class="custom-select form-control-border">
                                                                <option selected disabled value="">SMS tanlang</option>
                                                                @foreach($sms_text as $item)
                                                                    <option value="{{$item->id}}">{{$item->text}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleSelectBorder">SMS Srvice</label>
                                                            <select name="sms_service" id="" class="custom-select form-control-border" required>
                                                                <option selected disabled value="">Sms xizmat tanlang</option>
                                                                @foreach($sms_services as $sms)
                                                                    <option value="{{ $sms->name }}">{{ $sms->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleSelectBorder">Region</label>
                                                            <select name="region" id="" class="custom-select form-control-border">
                                                                <option selected disabled value="">Region tanlang</option>
                                                                @foreach($regions as $region)
                                                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-check">
                                                            <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                            <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                        </div>
                                                    </div>
                                                    <!-- /.card-body -->
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Yopish</button>
                                                    <button type="submit" class="btn btn-outline-light">Jo'natish</button>
                                                </div>

                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($phones as $phone)
                                <tr>
                                    <td class="py-1">
                                        {{($phones->currentpage()-1)*$phones->perpage()+($loop->index+1)}}
                                        <input name="sms_list[]" value="{{ $phone->id }}" id="{ $phone->id }}" type="checkbox">

                                    </td>
                                    <td class="py-1">{{ $phone->number }}</td>
                                    <td class="py-1">{{ $phone->region }}</td>
                                    <td class="py-1">
                                        {{ $phone->count }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </form>


                    </table>

                    <div>{{ $phones->appends($_GET)->links() }}</div>

                </div>
            </div>

        </div>

@endsection
